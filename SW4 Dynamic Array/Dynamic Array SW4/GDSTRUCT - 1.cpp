#include <iostream>
#include <string>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	UnorderedArray<int> grades(5);

	for (int i = 0; i < 10; i++)
		grades.push(i);

	cout << "Initial Values: " << endl;
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	cout << "\n\nRemove index 1" << endl;
	grades.remove(1);
	
	cout << "\n\nUpdated Values: " << endl;
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	cout << "Enter an element to search: ";
	int awit;
	cin >> awit;
	cout << "Searching for element " << awit << endl;
	int search = grades.linearSearch(awit);
	cout << "index number: " << search << endl;
	if (search == -1)
		cout << "The element does not exist!" << endl;

	system("pause");
}

