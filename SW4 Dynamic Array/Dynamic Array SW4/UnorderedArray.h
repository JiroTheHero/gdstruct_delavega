#pragma once

#include <assert.h>

template<class T>
class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~UnorderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T val)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = val;
		mNumElements++;
	}

	void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize)
		{
			return;
		}

		//shifitng elements from the right to the left
		for (int i = index; i < mMaxSize; i++)
		{
			mArray[i] = mArray[i + 1];
		}

		mMaxSize--;
		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize;
		}
	}

	int linearSearch(int element)
	{
		assert(mArray != NULL);

		for (int i = 0; i < mMaxSize; i++)
		{
			if (element == mArray[i])
				return i;
		}
		
		return -1;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;
	
	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};