#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

string getGuildName()
{
	cout << "What is the name of your guild?" << endl;
	string name;
	getline(cin, name);
	return name;
}

int getMemberAmount()
{
	cout << "How many members would you like to have in your guild?" << endl;
	int input;
	cin >> input;

	return input;
}

void inputNames(int amount, string *guild)
{
	cout << "Input the names of your guild members." << endl;
	for (int i = 0; i < amount; i++)
	{
		cin >> guild[i];
	}
}

void printNames(int amount, string *guild, string gName)
{
	cout << "GUILD: " << gName << endl;
	cout << "members: " << endl;
	for (int i = 0; i < amount; i++)
	{
		cout << "[" << i+1 << "] "<< guild[i] << endl;
	}
}

void renameMember(string *guild)
{
	cout << "Pick the # of the member you want to rename." << endl;
	int num;
	cin >> num;

	cout << "Rename " << guild[num-1] << " to: ";
	cin >> guild[num-1];
}

string addMember(int &amount, string *guild)
{
	cout << "adding a member..." << endl;
	amount++;
	string *temp = new string[amount];
	for (int i = 0; i < amount - 1 ; i++)
	{
		temp[i] = guild[i];
	}
	cout << "Input the name of the new member: ";
	cin >> temp[amount-1];
	cout << temp[amount - 1] << " has been added!" << endl;
	
	return *temp;
}

int main()
{
	string guildName;
	int amount;
	guildName = getGuildName();
	amount = getMemberAmount();

	string *guild = new string[amount];
	
	inputNames(amount, guild);
	cout << endl;
	printNames(amount, guild, guildName);

	cout << "What do you want to do with your guild?" << endl;
	cout << "[1] Rename a Member" << endl;
	cout << "[2] Rename a Member" << endl;
	cout << "[3] Add a Member" << endl;
	cout << "[4] Delete a Member" << endl;
	int input;
	cin >> input;
	if (input == 1)
	{
		printNames(amount, guild, guildName);
		renameMember(guild);
		printNames(amount, guild, guildName);
	}
	else if (input == 2)
	{
		printNames(amount, guild, guildName);
		renameMember(guild);
	}
	else if (input == 3)
	{
		*guild = addMember(amount, guild);
		printNames(amount, guild, guildName);
	}
	else if (input == 4)
	{
		cout << "remove member" << endl;
	}

	/*cout << endl;
	renameMember(guild);
	cout << endl;
	printNames(amount, guild, guildName);
	cout << endl;
	*guild = addMember(amount, guild);*/

	_getch();
	return 0;
}
