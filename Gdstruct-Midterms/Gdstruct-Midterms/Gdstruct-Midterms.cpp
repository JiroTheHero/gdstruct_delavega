#include "pch.h"
#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	while (true)
	{
		cout << "Unordered:	";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered:	";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << endl << endl;
		cout << "What do you want to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for Element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		int choice;
		cin >> choice;
		cout << endl;
		if (choice == 1)
		{
			cout << "Enter index to remove: ";
			int index;
			cin >> index;

			if (index < unordered.getSize() && unordered.getSize() != 0)
			{
				cout << "Delete at index " << index << endl;
				unordered.remove(index);
				ordered.remove(index);

				cout << "Unordered Contents:	";
				for (int i = 0; i < unordered.getSize(); i++)
					cout << unordered[i] << "   ";
				cout << "\nOrdered Contents:	";
				for (int i = 0; i < ordered.getSize(); i++)
					cout << ordered[i] << "   ";
			}
			else
				cout << "There is nothing to remove!";

			cout << endl;
		}
		else if (choice == 2)
		{
			cout << "\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
		}
		else if (choice == 3)
		{
			cout << "How many elements do you want to add: ";
			int size;
			cin >> size;

			cout << endl;
			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "Arrays have been expanded:" << endl;
			cout << "Unordered Contents:	";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered Contents:	";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			cout << endl;
		}

		cout << endl;
		system("pause");
		system("cls");
	}
}