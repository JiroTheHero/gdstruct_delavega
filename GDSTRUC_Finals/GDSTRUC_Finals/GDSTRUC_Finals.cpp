#include "pch.h"
#include <iostream>
#include <string.h>
#include "Stack.h"
#include "Queue.h"

using namespace std;

int main()
{
	cout << "Enter the size for element sets: ";
	int size;
	cin >> size;

	Stack<int> stack(size);
	Queue<int> queue(size);

	while (true)
	{
	cout << "What do you want to do? " << endl;
	cout << "[1] Push element" << endl;
	cout << "[2] Pop top element" << endl;
	cout << "[3] Print and pop all elements" << endl;
	int choice;
	cin >> choice;

		if (choice == 1)
		{
			cout << "Enter a number to add in the container: ";
			int input;
			cin >> input;
			queue.push(input);
			stack.push(input);
		}
		else if (choice == 2)
		{
			cout << "You have popped the top elements." << endl;
			queue.pop();
			stack.pop();
		}
		else if (choice == 3)
		{
			cout << "Queue Elements:" << endl;
			while (queue.getSize() > 0)
			{
				cout << queue.top() << endl;
				queue.pop();
			}
			cout << "Stack Elements:" << endl;
			while (stack.getSize() > 0)
			{
				cout << stack.top() << endl;
				stack.pop();
			}
		}

		if (choice != 3)
		{
			cout << endl;
			cout << "Top of element sets:" << endl;
			cout << "Queue: " << queue.top() << endl;
			cout << "Stack: " << stack.top() << endl;
		}

		system("pause");
		system("cls");
	}
}