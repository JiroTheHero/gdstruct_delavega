#include "pch.h"
#include <string>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <vector>

using namespace std;

void reverseWord(string word, int size)
{
	size--;
	if (size < 0)
	{
		return;
	}

	cout << word[size];
	reverseWord(word, size);
}

void pushOrderedArray(vector<int> &ordered, int value)
{
	if (ordered.empty() == true)
	{
		ordered.push_back(value);
		return;
	}

	int i;
	for (i = 0; i < ordered.size(); i++)
	{
		if (ordered[i] >= value)
		{
			break;
		}
	}
	
	ordered.insert(ordered.begin() + i, value);
}

void findTriplets(vector<int> unordered, int size, int sum)
{
	// di ko gets ahuhu
	return;
}

int binarySearch(vector<int> array, int value, int min, int max, int &counter)
{
	int mid = (min + max) / 2;

	if (min >= max)
	{
		return -1;
	}

	if (array[mid] == value)
	{
		return mid;
	}
	else if (array[mid] > value)
	{
		max = mid;
	}
	else if (array[mid] < value)
	{
		min = mid;
	}

	counter++;
	binarySearch(array, value, min, max, counter);
}

int main()
{
	srand(time(NULL));
	// #1
	string mWord;
	cout << "Input mga words: " << endl;
	getline(cin, mWord);
	int wordSize = mWord.length();
	cout << "The reverse of your word/s: " << endl;
	reverseWord(mWord, wordSize);
	cout << endl;
	system("pause");
	system("cls");

	//#2
	vector<int> unordered;
	int size = 8;
	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 10;
		unordered.push_back(rng);
	}

	cout << "Generated Array: " << endl;
	for (int i = 0; i < unordered.size(); i++)
	{
		cout << unordered[i] << " ";
	}
	cout << endl;

	int sum = 8;
	cout << "Looking for triplets with the sum of " << sum << endl;
	findTriplets(unordered, size, sum);

	system("pause");
	system("cls");

	//#3
	vector<int> ordered;
	for (int i = 0; i < 10; i++)
	{
		int rng = rand() % 101;
		pushOrderedArray(ordered, rng);
	}

	cout << "Generated Array: " << endl;
	for (int i = 0; i < ordered.size(); i++)
	{
		cout << ordered[i] << " ";
	}
	cout << endl;

	int input;
	cout << "Enter a number to search: ";
	cin >> input;
	int counter = 0;
	int result = binarySearch(ordered, input, 0, ordered.size() - 1, counter);

	if (result > 0)
	{
		cout << input << " is found." << endl;
		cout << "The algorithm took " << counter << " steps to find the number." << endl;
	}
	else
	{
		cout << "The number does not exist in the array." << endl;
	}

	system("pause");
	return 0;
}