#include "pch.h"
#include <iostream>
#include <string.h>

using namespace std;

void printReverseNumber(int from)
{
	cout << from << " ";
	if (from > 0)
	{
		from--;
		printReverseNumber(from);
	}
	return;
}

int printSumOfDigits(int num, int sum)
{
	if (num <= 0)
	{
		return sum;
	}

	sum += num % 10;
	num = num / 10;
	return printSumOfDigits(num, sum);
}

void printFibonacciSequence(int term, int prevNum, int currNum)
{
	if (term <= 0)
	{
		return;
	}

	cout << currNum << " ";
	int sum = prevNum + currNum;
	prevNum = currNum;
	currNum = sum;

	printFibonacciSequence(term - 1, prevNum, currNum);
}

bool ifPrime(int num, int div)
{
	if ((div == num) || (num == 2 || num == 1))
	{
		return true;
	}


	if ((num % div == 0) || (num == 0))
	{
		return false;
	}

	return ifPrime(num, div++);
}

int main()
{
	int input1;
	cout << "input a number to print it from that to 1: ";
	cin >> input1;
	printReverseNumber(input1);
	cout << endl;

	int input2;
	cout << "input a number find the sum of its digits: ";
	cin >> input2;
	cout << printSumOfDigits(input2, 0);
	cout << endl;

	int input3;
	cout << "input the nth term of the Fibonacci Sequence: ";
	cin >> input3;
	printFibonacciSequence(input3, 0, 1);
	cout << endl;

	int input4;
	cout << "input a number to check if it is a prime number: ";
	cin >> input4;
	bool isItPrime = ifPrime(input4, 1);
	if (isItPrime)
	{
		cout << input4 << " is a prime number.";
	}
	else
	{
		cout << input4 << " is not a prime number.";
	}
	cout << endl;

	system("pause");
}